# Contributing Guidelines

### Learn how to Fork a repo , make changes and ask a maintainer to review and merge

## Creating a Pull Request
If you want to contribute to other projects on Github. How do you do it?<br>
When you want to work on another's GitHub project, the first step is to fork a repo.<br>


This creates a new copy of the repo under your Github user account.
- Clone this Github repo. Open up the GitBash/Command Line and type in:<br>

```bash
git clone git@github.com:jeyserHiro/hiro_connect.git
cd hiro_connect

git remote add upstream git@github.com:<your username>/hiro_connect.git

git checkout -b <IssueNumber>
```

- Start Coding. Make apt commits with proper commit messages. Always use ``` git status``` to see that you have not made changes on the file you were supposed not to

```git
git add .
git commit -m "<Your Message>"
git push upstream <IssueNumber>
```

- Once you push the changes to your repo, Go to the original repository
- Open a pull request by clicking the Create pull request button. This allows the repo's maintainers to reviews your work. From here, they can merge it if it is good, or they may ask you for some changes.

